import Foundation

///
///
///
extension URLSession {

	///
	///
	///
	func jsonDataTask<T: Decodable>(with urlRequest: URLRequest, completion: @escaping (T?, Error?)->Void) -> URLSessionDataTask {
		return dataTask(with: urlRequest) { data, response, error in
			
			guard let httpResponse = (response as? HTTPURLResponse) else {
				return completion(nil, APIRequestLoaderError.unexpectedURLResponse(response))
			}

			guard httpResponse.statusCode == 200 else {
				return completion(nil, APIRequestLoaderError.httpResponseNotOK(httpResponse.statusCode))
			}

			guard let data = data else {
				return completion(nil, APIRequestLoaderError.emptyResponseData)
			}

			do {
				let decodedJson = try JSONDecoder().decode(T.self, from: data)	
				completion(decodedJson, error)
			}
			catch {
				print(String(data: data, encoding: .utf8)!)
				completion(nil, APIRequestLoaderError.decodingError(error))	
			}
		}
	}
}