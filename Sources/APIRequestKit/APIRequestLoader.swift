import Foundation

///
///
///
public enum APIRequestLoaderError: Error {
	case unexpectedURLResponse(URLResponse?)
	case httpResponseNotOK(Int)
	case emptyResponseData
	case decodingError(Error)
}

///
///
///
public class APIRequestLoader<T: APIRequest> {

	///
	///
	///
	private var apiRequest: T

	///
	///
	///
	private var urlSession: URLSession

	///
	///
	///
	public init(apiRequest: T, urlSession: URLSession = .shared) {
		self.apiRequest = apiRequest
		self.urlSession = urlSession
	}

	///
	///
	///
	public func load(requestData: T.RequestData, completion: @escaping (T.ResponseData?, Error?)->Void) {
		
		do {
			let urlRequest = try apiRequest.buildURLRequest(input: requestData)
			urlSession.jsonDataTask(with: urlRequest) { completion($0, $1) }.resume()
		}
		catch { completion(nil, error) }
	}
}
