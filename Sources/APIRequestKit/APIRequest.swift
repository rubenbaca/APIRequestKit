import Foundation

public protocol APIRequest {
	associatedtype RequestData
	associatedtype ResponseData: Decodable
	func buildURLRequest(input requestData: RequestData) throws -> URLRequest
}
