import XCTest

import APIRequestKitTests

var tests = [XCTestCaseEntry]()
tests += APIRequestKitTests.allTests()
XCTMain(tests)