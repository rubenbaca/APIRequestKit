import Foundation 
@testable import APIRequestKit	

struct MockAPIRequest: APIRequest {

	typealias ResponseData = [String:String]
	typealias RequestData = [String:String]

	static var mockServiceURL = URL(string: "https://example.com/")!
	
	func buildURLRequest(input requestData: RequestData) throws -> URLRequest {

		var urlComponents = URLComponents(url: MockAPIRequest.mockServiceURL, resolvingAgainstBaseURL: true)!
		urlComponents.queryItems = []
		
		for (param, value) in requestData {
			urlComponents.queryItems?.append(URLQueryItem(name: param, value: value))
		}

		print(urlComponents.url!)
		return URLRequest(url: urlComponents.url!)
	}
}