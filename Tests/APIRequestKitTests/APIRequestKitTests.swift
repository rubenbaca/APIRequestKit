import XCTest
@testable import APIRequestKit

///
///
///
final class APIRequestKitTests: XCTestCase {

	///
	///
	///
    private var loader: APIRequestLoader<MockAPIRequest>!

    ///
	///
	///
    override func setUp() {
    	let configuration = URLSessionConfiguration()
		configuration.protocolClasses = [MockURLProtocol.self]
		let urlSession = URLSession(configuration: configuration)
		loader = APIRequestLoader(apiRequest: MockAPIRequest(), urlSession: urlSession)
    }

    ///
    ///
    ///
    private let semaphore = DispatchSemaphore(value: 0)

    ///
	/// We send some arguments, the server replies with 200-OK and a valid/parsed response
	///
    func testLoaderSuccessWithData() throws {

		let mockRequestInput = ["one":"1", "two":"2"]
		let mockResponse = try JSONEncoder().encode(mockRequestInput) // Response is a JSON representation of the input

		MockURLProtocol.requestHandler = { urlRequest in
			XCTAssertEqual(urlRequest.url?.query?.contains("one=1"), true)
			XCTAssertEqual(urlRequest.url?.query?.contains("two=2"), true)
			XCTAssertEqual(urlRequest.url?.host, MockAPIRequest.mockServiceURL.host)

			return(HTTPURLResponse(url: urlRequest.url!, statusCode: 200, httpVersion: nil, headerFields: nil)!, mockResponse)
		}

		loader.load(requestData: mockRequestInput) { response, error in
			XCTAssert(error == nil)
			XCTAssertEqual(response, mockRequestInput)
			self.finishedAsyncTask()
		}

		waitAsyncTask()
    }

    ///
    /// Server replies with an HTTP error, so the APIRequestLoader.load() replies with an error
    ///
    func testLoaderHTTPError() {
    	MockURLProtocol.requestHandler = { urlRequest in
			return(HTTPURLResponse(url: urlRequest.url!, statusCode: 500, httpVersion: nil, headerFields: nil)!, Data())
    	}

    	loader.load(requestData: [:]) { response, error in
    		XCTAssert(error != nil)
    		XCTAssert(response == nil)
    		self.finishedAsyncTask()
    	}
    	waitAsyncTask()
    }

    ///
    /// Server replies with 200-OK but with no data, meanining that APIRequestLoader.load() must 
    /// return an error (failed to parse empty data)
    ///
    func testLoaderErrorEmptyDataFromServer() {
        MockURLProtocol.requestHandler = { urlRequest in
            return(HTTPURLResponse(url: urlRequest.url!, statusCode: 200, httpVersion: nil, headerFields: nil)!, Data())
        }

        loader.load(requestData: [:]) { response, error in
            XCTAssert(error != nil)
            XCTAssert(response == nil)
            self.finishedAsyncTask()
        }
        waitAsyncTask()
    }

    ///
    ///
    ///
    private func waitAsyncTask() {
    	semaphore.wait()
    }

    ///
    ///
    ///
    private func finishedAsyncTask() {
    	semaphore.signal()
    }

    ///
	///
	///
    static var allTests = [
        ("testLoaderSuccessWithData", testLoaderSuccessWithData),
        ("testLoaderHTTPError", testLoaderHTTPError),
        ("testLoaderErrorEmptyDataFromServer", testLoaderErrorEmptyDataFromServer),
    ]
}
